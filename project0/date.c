//
//  date.c
//  Name: Nate Boyd
//  DuckID: 950426693
//  CIS 415 Project 0
//  This is my own work
//

#include "date.h"
#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <string.h>

struct date {
    char *year;
    char *month;
    char *day;
};

/*
 * date_create creates a Date structure from `datestr`
 * `datestr' is expected to be of the form "dd/mm/yyyy"
 * returns pointer to Date structure if successful,
 *         NULL if not (syntax error)
 */

/*
 The constructor for this ADT is date_create(); it converts a datestring in the
 format “dd/mm/yyyy” to a Date structure. You will have to use malloc() to allocate
 this Date structure to return to the user.
 */
Date *date_create(char *datestr)
{
    
    int size = 0;
    char *str = datestr;
    
    Date *date = (Date *)malloc (sizeof(Date));
    
    if (date == NULL){
        return NULL;
    }
    
    //!!!!!!!Edit!!!!!!!!
    char *day = (char *)malloc(3*sizeof(char));
    char *month = (char *)malloc(3*sizeof(char));
    char *year = (char *)malloc(5*sizeof(char));
    
    
    while ( *str++ != '\0')
        size++;
    
    memcpy(day, datestr, 2 );
    day[2] = '\0';
    
    memcpy( month, datestr + 3, 2 );
    month[2] = '\0';
    
    memcpy( year, datestr + 6, 4 );
    year[4] = '\0';
    
    date->day = day;
    date->month = month;
    date->year = year;
    
    return date;
}

/*
 * date_duplicate creates a duplicate of `d'
 * returns pointer to new Date structure if successful,
 *         NULL if not (memory allocation failure)
 */

/*
 date_duplicate() is known as a copy constructor; it duplicates the Date argument
 on the heap (using malloc()) and returns it to the user.
 */

Date *date_duplicate(Date *d){
    
    Date *new_d;
    new_d = (Date *)malloc(sizeof(Date));
    
    new_d->day = d->day;
    new_d->month = d->month;
    new_d->year = d->year;
    
    return new_d;
}

/*
 * date_compare compares two dates, returning <0, 0, >0 if
 * date1<date2, date1==date2, date1>date2, respectively
 */
int date_compare(Date *date1, Date *date2)
{
    //consider using sscanf
    
    int temp;
    int date1NumYear = atoi(date1->year);
    int date2NumYear = atoi(date2->year);
    int date1NumMonth = atoi(date1->month);
    int date2NumMonth = atoi(date1->month);
    int date1NumDay = atoi(date1->day);
    int date2NumDay = atoi(date2->day);
    
    
    //same year?
    temp = date1NumYear - date2NumYear;
    if (temp == 0){
        //same month?
        temp = date1NumMonth - date2NumMonth;
        
        if (temp == 0){
            //return day diffrence
            temp = date1NumDay - date2NumDay;
                return temp;
        }
        //month diffrence
        else
            return temp;
    }
    //year diffrence
    else
        return temp;
    
    return 0;
}


/*
 * date_destroy returns any storage associated with `d' to the system
 */
void date_destroy(Date *d){
    
    free(d->day);
    free(d->month);
    free(d->year);
    free(d);
    
}



