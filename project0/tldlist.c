//
//  tldlist.c
//  Name: Nate Boyd
//  DuckID: 950426693
//  CIS 415 Project 0
//  This is mostly my work, AVL tree reference material taken from: http://www.thecrazyprogrammer.com/2014/03/c-program-for-avl-tree-implementation.html & http://www.geeksforgeeks.org/avl-tree-set-1-insertion/
//  Itteration troubleshooting help from Stack Overflow: http://stackoverflow.com/questions/1585186/array-of-pointers-initialization

//You must implement tldlist.c as a balanced binary search tree (AVL), based upon the Adelson-Velskii and Landis algorithm.

#include "tldlist.h"
#include "date.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <stdbool.h>


static TLDNode *addTldNode(TLDList *tld, char *temptld, TLDNode *node);
static TLDNode *newTldNode(char *temptld);
static void iterAdd(TLDIterator *iter, TLDNode *node, int *i);

static int getHeight(TLDNode *node);
static int heightDiffrence(TLDNode *node);


static TLDNode *RR(TLDNode *node);
static TLDNode *RL(TLDNode *node);
static TLDNode *LL(TLDNode *node);
static TLDNode *LR(TLDNode *node);
static TLDNode *balance(TLDNode *node);

struct tldnode {
    char *temptld;
    long count;
    
    TLDNode *leftChild;
    TLDNode *rightChild;
    
};

struct tldlist {
    long count;
    long size;
    
    TLDNode *root;
    Date *begin;
    Date *end;

};


struct tlditerator {
    int i;
    long size;
    
    TLDList *tld;
    TLDNode **next;
};

/*
 tldlist_create() creates a TLDList which can be used to store the counts of log
 entries against TLD strings; the begin and end date arguments enable filtering of added
 entries to be in the preferred date range.
 */

TLDList *tldlist_create(Date *begin, Date *end)
{
    TLDList *tldlist = malloc(sizeof(TLDList));
    if (tldlist == NULL){
        return tldlist;
    }
    else
    {
        tldlist->count = 0;
        tldlist->size = 0;
        tldlist->root = NULL;
        tldlist->begin = begin;
        tldlist->end = end;
    }
    
    return tldlist;
    
}



/*
 tldlist_add() will count the log entry if the associated date is within the preferred
 data range.
 */

int tldlist_add(TLDList *tld, char *hostname, Date *d) {
    // check if it's within the tld dates
    if ((date_compare(tld->end, d) < 0) || (date_compare(tld->begin, d) > 0)){
        return 0;
    }
    // using period as the delimiter, using strrchr to get the last instance of .
    char *domain = strrchr(hostname, '.') + 1;
    
    //puts all of the TLD into lower case
    for(int i=0; i < sizeof(domain); i++){
        domain[i] = tolower(domain[i]);
    }
    
    char *tempTLD = (char *)malloc(sizeof(domain));
    
    //Nate Note: make sure this works properly!
    strncpy(tempTLD, domain,sizeof(domain));
    
    // pass the result to AVL tree
    tld->root = addTldNode(tld, tempTLD, tld->root);
    tld->count++;
    return 1;
}


// Recursive insert function for AVL
static TLDNode *addTldNode(TLDList *tld, char *temptld, TLDNode *node) {
    
    int compareResult = strcmp(temptld, node->temptld);
    
    //if there is no root or we have reached the leaf we're looking for
    if (node == NULL)
    {
        node = newTldNode(temptld);
        tld->root = node;
        tld->size++;
        return node;
    }
    
    //moving right, recurse, and rebalance
    else if (compareResult > 0)
    {
        node->rightChild = addTldNode(tld, temptld, node->rightChild);
        node = balance(node);
    }
    //move left, recurse, and rebalance
    else if (compareResult < 0)
    {
        node->leftChild = addTldNode(tld, temptld, node->leftChild);
        node = balance(node);
    }
    else
    {
        free(temptld);
        node->count++;
    }
    
    return node;
}



/*
 tldlist_count() returns the number of log entries that have been counted in the
 list.
 */

long tldlist_count(TLDList *tld)
{
    return tld->count;
}



/*
 tldlist_iter_create() creates an iterator to enable you to iterate over the
 entries, independent of the data structure chosen for representing the list.
 */
TLDIterator *tldlist_iter_create(TLDList *tld) {
    
    TLDIterator *iter = (TLDIterator *)malloc(sizeof(TLDIterator));
    
    if (iter != NULL)
    {
        int i = 0;
        
        iter->tld = tld;
        iter->size = tld->size;
        iter->i = 0;
        
        iter->next = (TLDNode **)malloc(sizeof(TLDNode *) * iter->size);
        
        if (iter->next == NULL)
        {
            tldlist_iter_destroy(iter);
            return NULL;
        }

        iterAdd(iter, iter->tld->root, &i);
        return iter;
    }
    else
    {
        free(iter);
        return NULL;
    }
    
    return NULL;
}

static void iterAdd(TLDIterator *iter, TLDNode *node, int *i)
{
    
    if (node->leftChild)
        iterAdd(iter, node->leftChild, i);
    
    *(iter->next + (*i)++) = node;
    
    if (node->rightChild)
        iterAdd(iter, node->rightChild, i);
}

/*
 * tldlist_iter_next returns the next element in the list; returns a pointer
 * to the TLDNode if successful, NULL if no more elements to return
 */
TLDNode *tldlist_iter_next(TLDIterator *iter)
{
    if (iter->i != iter->size){
        return *(iter->next + iter->i++);
    }
    return NULL;
}

/*
 * tldlist_iter_destroy destroys the iterator specified by `iter'
 */
void tldlist_iter_destroy(TLDIterator *iter) {
    
    for (int i = 0; i < iter->size; i++){
        free(iter->next[i]->temptld);
        free(iter->next[i]);
        i++;
    }
    
    free(iter->next);
    free(iter);
}

/*
 * tldnode_tldname returns the tld associated with the TLDNode
 */
char *tldnode_tldname(TLDNode *node)
{
    return node->temptld;
}

/*
 * tldnode_count returns the number of times that a log entry for the
 * corresponding tld was added to the list
 */
long tldnode_count(TLDNode *node)
{
    return node->count;
}

static TLDNode *newTldNode(char *temptld)
{
    TLDNode *node = (TLDNode *)malloc(sizeof(TLDNode));
    
    if (node != NULL)
    {
        node->leftChild = NULL;
        node->rightChild = NULL;
        node->temptld = temptld;
        node->count = 1;
        
        return node;
    }

    else
    {
        free(node);
        return NULL;
    }
}

/*
 * tldlist_destroy destroys the list structure in `tld'
 *
 * all heap allocated storage associated with the list is returned to the heap
 */
void tldlist_destroy(TLDList *tld)
{
    free(tld);
}

//function for returning current tree height
static int getHeight(TLDNode *node)
{
    
    int leftHeight;
    int rightHeight;
    int height = 0;
    int leaf;
    if (node == NULL){
        return height;
    }
    else
    {
        leftHeight = getHeight(node->leftChild);
        rightHeight = getHeight(node->rightChild);
        
        if (leftHeight > rightHeight)
        {
            leaf = leftHeight;
        }
        else
        {
            leaf = rightHeight;
        }
        height= 1+leaf;
    }
    return height;
}

//function for the height diffrence between branches
static int heightDiffrence(TLDNode *node)
{
    int diffrence;
    int rightHeight;
    int leftHeight;
    
    rightHeight = getHeight(node->rightChild);
    leftHeight = getHeight(node->leftChild);
    diffrence = leftHeight - rightHeight;
    
    return diffrence;
}

// Rotation functions for AVL balancing
static TLDNode *RR(TLDNode *node)
{
    TLDNode *temp;
    
    temp = node->rightChild;
    node->rightChild = temp->leftChild;
    temp->leftChild = node;
    
    return temp;
}

static TLDNode *RL(TLDNode *node)
{
    TLDNode *temp;
    
    temp =node->rightChild;
    node->rightChild = LL(temp);
    
    return RR(node);
}

static TLDNode *LL(TLDNode *node)
{
    TLDNode *temp;
    
    temp = node->leftChild;
    node->leftChild = temp->rightChild;
    temp->rightChild = node;
    
    return temp;
}

static TLDNode *LR(TLDNode *node)
{
    TLDNode *temp;
    
    temp = node->leftChild;
    node->leftChild = RR(temp);
    
    return LL(node);
}

static TLDNode *balance(TLDNode *node)
{
    int balancingDiff;
    balancingDiff = heightDiffrence(node);
    if (balancingDiff > 1)
    {
        if (heightDiffrence(node->leftChild) < 0)
        {
            node = LR(node);
        }
        else
        {
            node = LL(node);
        }
    }
    else if (balancingDiff < -1)
    {
        if (heightDiffrence(node->rightChild) < 0)
        {
            node = RR(node);
        }
        else
        {
            node = RL(node);
        }
    }
    return node;
}
